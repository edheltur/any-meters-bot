﻿using System.Threading.Tasks;
using AnyMeterBot.Service.Services;
using Microsoft.AspNetCore.Mvc;
using Telegram.Bot.Types;

namespace AnyMeterBot.Service.Controllers
{
    [Route("api/[controller]")]
    public class UpdateController : Controller
    {
        private readonly IHandler<Update> updateHandler;

        public UpdateController(IHandler<Update> updateHandler)
        {
            this.updateHandler = updateHandler;
        }


        [HttpPost]
        public async Task<IActionResult> Post([FromBody]Update update)
        {
            await updateHandler.Handle(update);
            return Ok();
        }
    }
}