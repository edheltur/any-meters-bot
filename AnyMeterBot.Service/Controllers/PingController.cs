﻿using Microsoft.AspNetCore.Mvc;
using Telegram.Bot;

namespace AnyMeterBot.Service.Controllers
{
    [Route("api/[controller]")]
    public class PingController : Controller
    {
        private readonly ITelegramBotClient telegramBotClient;

        public PingController(ITelegramBotClient telegramBotClient)
        {
            this.telegramBotClient = telegramBotClient;
        }

        [HttpGet]
        public ActionResult Get() => Ok();
    }
}