﻿using System.Collections.Generic;
using JetBrains.Annotations;

namespace AnyMeterBot.Service.DataLayer
{
    public class ReportEntry
    {
        public int ReportEntryId { get; set; }
        [CanBeNull]
        public string Name { get; set; }
        public double? Value { get; set; }
        public Unit Unit { get; set; }
        public IList<ReportCallback> Callbacks { get; set; }

        public ReportEntry()
        {
            Callbacks = new List<ReportCallback>();
        }
    }
}