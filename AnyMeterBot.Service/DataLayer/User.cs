﻿using System.Collections.Generic;
using JetBrains.Annotations;

namespace AnyMeterBot.Service.DataLayer
{
    public class User
    {
        public int UserId { get; set; }
        public int TelegramUserId { get; set; }
        public int? EmailSettingsId { get; set; }
        [CanBeNull]
        public EmailSettings EmailSettings { get; set; }
        public IList<Report> Reports { get; set; }
    }
}