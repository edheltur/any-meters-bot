﻿namespace AnyMeterBot.Service.DataLayer
{
    public enum ReportCallbackAction
    {
        AddEntry = 1,
        Send = 2,
        EditName = 3,
        EditValue = 4,
        EditUnit = 5,
        RemoveEntry = 6,
    }
}