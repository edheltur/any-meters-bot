﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using Microsoft.IdentityModel.Clients.ActiveDirectory;

namespace AnyMeterBot.Service.DataLayer
{
    public class Report
    {
        public int ReportId { get; set; }
        
        public User Creator { get; set; }
        public int CreatorId { get; set; }
                
        public int TelegramMessageId { get; set; }
        public long TelegramChatId { get; set; }
        public DateTime CreationDate { get; set; }
        public IList<ReportEntry> Entries { get; set; }

        public IList<ReportCallback> Callbacks { get; set; }

        
        public Report()
        {
            Callbacks = new List<ReportCallback>();
            Entries = new List<ReportEntry>();
        }
    }
    
    

}