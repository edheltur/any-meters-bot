﻿using System;

namespace AnyMeterBot.Service.DataLayer
{
    public class ReportCallback
    {
        public Guid ReportCallbackId { get; set; }
        public ReportCallbackAction Action { get; set; }
    }
}