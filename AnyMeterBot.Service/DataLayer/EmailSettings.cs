﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using JetBrains.Annotations;

namespace AnyMeterBot.Service.DataLayer
{
    public class EmailSettings
    {
        public int EmailSettingsId { get; set; }

        [CanBeNull]
        public MailAddress To { get; set; }
        
        [CanBeNull]
        public MailAddress From { get; set; }
 
        
    }
}