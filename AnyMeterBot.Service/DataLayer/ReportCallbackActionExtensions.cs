﻿using JetBrains.Annotations;

namespace AnyMeterBot.Service.DataLayer
{
    public static class ReportCallbackActionExtensions
    {
        [CanBeNull]
        public static string GetTitle(this ReportCallbackAction action, [CanBeNull] ReportEntry entry)
        {
            switch (action)
            {
                case ReportCallbackAction.AddEntry:
                    return "Добавить запись";
                case ReportCallbackAction.Send:
                    return "Отправить отчёт";
                case ReportCallbackAction.EditName:
                    return entry?.Name ?? "?";
                case ReportCallbackAction.EditValue:
                    return entry?.Value?.ToString() ?? "?";
                case ReportCallbackAction.EditUnit:
                    return entry?.Unit.GetTitle();
                case ReportCallbackAction.RemoveEntry:
                    return "\u274C";
                default:
                    return null;
            }
        }
    }
}