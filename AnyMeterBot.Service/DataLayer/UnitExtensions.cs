﻿using System;
using System.Linq;

namespace AnyMeterBot.Service.DataLayer
{
    public static class UnitExtensions
    {
        private static readonly Unit[] AllUnits = Enum
            .GetValues(typeof(Unit)).Cast<Unit>()
            .ToArray();

        public static Unit Next(this Unit unit)
        {
            var index = Array.IndexOf(AllUnits, unit);
            return AllUnits[(index + 1)% AllUnits.Length];
        }

        public static string GetTitle(this Unit unit)
        {
            switch (unit)
            {
                case Unit.kWh:
                    return "кВт⋅ч";
                case Unit.m3:
                    return "m³";
                default:
                    return string.Empty;
            }
        }
    }
}