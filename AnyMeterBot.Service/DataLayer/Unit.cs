﻿namespace AnyMeterBot.Service.DataLayer
{
    public enum Unit
    {
        kWh = 1,
        m3 = 2
    }
}