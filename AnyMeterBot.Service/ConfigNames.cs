﻿namespace AnyMeterBot.Service
{
    static class ConfigNames
    {
        public const string CONNECTION_STRING = "SERVICE_CONNECTION_STRING";
        public const string TELEGRAM_BOT_TOKEN = "SERVICE_TELEGRAM_BOT_TOKEN";
        public const string ELASTIC_EMAIL_API_KEY = "ELASTIC_EMAIL_API_KEY";
        
        public const string USERS_ALLOWED_TO_SEND_EMAIL = "UsersAllowedToSendEmail";
    }
}