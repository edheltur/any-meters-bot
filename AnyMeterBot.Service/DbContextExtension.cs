﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.Extensions.Logging;

namespace AnyMeterBot.Service
{
    public class DbContextMigrator<TContext> : IDbContextMigrator where TContext : DbContext
    {
        private readonly TContext context;
        private readonly IHistoryRepository historyRepository;
        private readonly IMigrator migrator;
        private readonly ILogger<DbContextMigrator<TContext>> logger;

        public DbContextMigrator(TContext context, ILogger<DbContextMigrator<TContext>> logger)
        {
            this.context = context;
            this.logger = logger;
            this.historyRepository = context.GetService<IHistoryRepository>();
            this.migrator = context.GetService<IMigrator>();
        }

        private async Task WaitForDb(CancellationToken cancellationToken)
        {
            logger.LogInformation("Waiting for DB ...");
            while (true)
            {
                try
                {
                    await historyRepository.ExistsAsync(cancellationToken);
                    break;
                }
                catch (System.Net.Sockets.SocketException)
                {
                    await Task.Delay(200, cancellationToken);
                }
                catch (OperationCanceledException)
                {
                    break;
                }
            }
            logger.LogInformation("DB is ready!");
        }


        public async Task WaitForDbAndMigrate(CancellationToken cancellationToken = default)
        {
            await WaitForDb(cancellationToken);
            await migrator.MigrateAsync(null, cancellationToken);
        }
    }
}