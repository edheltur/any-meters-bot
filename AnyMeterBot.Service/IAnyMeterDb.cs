﻿using System.Threading;
using System.Threading.Tasks;
using AnyMeterBot.Service.DataLayer;
using Microsoft.EntityFrameworkCore;

namespace AnyMeterBot.Service
{
    public interface IAnyMeterDb
    {
        DbSet<Report> Reports { get; }
        DbSet<ReportCallback> ReportCallbacks { get; }
        DbSet<User> Users { get; }

        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
    }
}