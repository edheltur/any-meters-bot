﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AnyMeterBot.Service.Migrations
{
    public partial class RemoveTelegramUserId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TelegramUserId",
                table: "Reports");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "TelegramUserId",
                table: "Reports",
                nullable: true);
        }
    }
}
