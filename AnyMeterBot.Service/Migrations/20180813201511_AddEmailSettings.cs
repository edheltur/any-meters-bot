﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace AnyMeterBot.Service.Migrations
{
    public partial class AddEmailSettings : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Reports_ReportEntry_CurrentEntryReportEntryId",
                table: "Reports");

            migrationBuilder.DropIndex(
                name: "IX_Reports_CurrentEntryReportEntryId",
                table: "Reports");

            migrationBuilder.DropColumn(
                name: "CurrentEntryReportEntryId",
                table: "Reports");

            migrationBuilder.AddColumn<int>(
                name: "CreatorId",
                table: "Reports",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "EmailSettings",
                columns: table => new
                {
                    EmailSettingsId = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    To = table.Column<string>(nullable: true),
                    From = table.Column<string>(nullable: true),
                    ReplyTo = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmailSettings", x => x.EmailSettingsId);
                });

            migrationBuilder.Sql("DELETE FROM \"ReportCallbacks\" WHERE 1 = 1;");
            migrationBuilder.Sql("DELETE FROM \"ReportEntry\" WHERE 1 = 1;");
            migrationBuilder.Sql("DELETE FROM \"Reports\" WHERE 1 = 1;");
            
            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    TelegramUserId = table.Column<int>(nullable: false),
                    EmailSettingsId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.UserId);
                    table.ForeignKey(
                        name: "FK_Users_EmailSettings_EmailSettingsId",
                        column: x => x.EmailSettingsId,
                        principalTable: "EmailSettings",
                        principalColumn: "EmailSettingsId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Reports_CreatorId",
                table: "Reports",
                column: "CreatorId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_EmailSettingsId",
                table: "Users",
                column: "EmailSettingsId");

            migrationBuilder.AddForeignKey(
                name: "FK_Reports_Users_CreatorId",
                table: "Reports",
                column: "CreatorId",
                principalTable: "Users",
                principalColumn: "UserId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Reports_Users_CreatorId",
                table: "Reports");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "EmailSettings");

            migrationBuilder.DropIndex(
                name: "IX_Reports_CreatorId",
                table: "Reports");

            migrationBuilder.DropColumn(
                name: "CreatorId",
                table: "Reports");

            migrationBuilder.AddColumn<int>(
                name: "CurrentEntryReportEntryId",
                table: "Reports",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Reports_CurrentEntryReportEntryId",
                table: "Reports",
                column: "CurrentEntryReportEntryId");

            migrationBuilder.AddForeignKey(
                name: "FK_Reports_ReportEntry_CurrentEntryReportEntryId",
                table: "Reports",
                column: "CurrentEntryReportEntryId",
                principalTable: "ReportEntry",
                principalColumn: "ReportEntryId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
