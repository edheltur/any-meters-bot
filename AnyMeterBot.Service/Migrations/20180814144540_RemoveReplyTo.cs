﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AnyMeterBot.Service.Migrations
{
    public partial class RemoveReplyTo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ReplyTo",
                table: "EmailSettings");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ReplyTo",
                table: "EmailSettings",
                nullable: true);
        }
    }
}
