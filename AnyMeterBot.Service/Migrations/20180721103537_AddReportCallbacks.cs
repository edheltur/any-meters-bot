﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AnyMeterBot.Service.Migrations
{
    public partial class AddReportCallbacks : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "TelegramMessageId",
                table: "Reports",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "ReportCallbacks",
                columns: table => new
                {
                    ReportCallbackId = table.Column<Guid>(nullable: false),
                    Action = table.Column<int>(nullable: false),
                    ReportId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ReportCallbacks", x => x.ReportCallbackId);
                    table.ForeignKey(
                        name: "FK_ReportCallbacks_Reports_ReportId",
                        column: x => x.ReportId,
                        principalTable: "Reports",
                        principalColumn: "ReportId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ReportCallbacks_ReportId",
                table: "ReportCallbacks",
                column: "ReportId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ReportCallbacks");

            migrationBuilder.DropColumn(
                name: "TelegramMessageId",
                table: "Reports");
        }
    }
}
