﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AnyMeterBot.Service.Migrations
{
    public partial class AddReportCallabcks : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ReportEntryId",
                table: "ReportCallbacks",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ReportCallbacks_ReportEntryId",
                table: "ReportCallbacks",
                column: "ReportEntryId");

            migrationBuilder.AddForeignKey(
                name: "FK_ReportCallbacks_ReportEntry_ReportEntryId",
                table: "ReportCallbacks",
                column: "ReportEntryId",
                principalTable: "ReportEntry",
                principalColumn: "ReportEntryId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ReportCallbacks_ReportEntry_ReportEntryId",
                table: "ReportCallbacks");

            migrationBuilder.DropIndex(
                name: "IX_ReportCallbacks_ReportEntryId",
                table: "ReportCallbacks");

            migrationBuilder.DropColumn(
                name: "ReportEntryId",
                table: "ReportCallbacks");
        }
    }
}
