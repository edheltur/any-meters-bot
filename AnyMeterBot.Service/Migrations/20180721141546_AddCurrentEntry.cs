﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AnyMeterBot.Service.Migrations
{
    public partial class AddCurrentEntry : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CurrentEntryReportEntryId",
                table: "Reports",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Reports_CurrentEntryReportEntryId",
                table: "Reports",
                column: "CurrentEntryReportEntryId");

            migrationBuilder.AddForeignKey(
                name: "FK_Reports_ReportEntry_CurrentEntryReportEntryId",
                table: "Reports",
                column: "CurrentEntryReportEntryId",
                principalTable: "ReportEntry",
                principalColumn: "ReportEntryId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Reports_ReportEntry_CurrentEntryReportEntryId",
                table: "Reports");

            migrationBuilder.DropIndex(
                name: "IX_Reports_CurrentEntryReportEntryId",
                table: "Reports");

            migrationBuilder.DropColumn(
                name: "CurrentEntryReportEntryId",
                table: "Reports");
        }
    }
}
