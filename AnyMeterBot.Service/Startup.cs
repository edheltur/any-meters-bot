﻿using System.Linq;
using System.Threading.Tasks;
using AnyMeterBot.Service.Services;
using AnyMeterBot.Service.Services.Mailing;
using AnyMeterBot.Service.Services.ReportActionHandlers;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Telegram.Bot;
using Telegram.Bot.Types;
using IApplicationLifetime = Microsoft.AspNetCore.Hosting.IApplicationLifetime;

namespace AnyMeterBot.Service
{
    public class Startup
    {
        private readonly ILogger<Startup> logger;
        private readonly IConfiguration config;

        public Startup(ILogger<Startup> logger, IConfiguration config)
        {
            this.logger = logger;
            this.config = config;
        }

        [UsedImplicitly]
        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddEntityFrameworkNpgsql()
                .AddDbContext<AnyMeterBotDataContext>(options =>
                    options.UseNpgsql(config[ConfigNames.CONNECTION_STRING]));

            services.AddMvc();

            services
                .AddTransient<IDateTime, UtcDateTime>()
                .AddScoped<IAnyMeterDb, AnyMeterBotDataContext>()
                .AddSingleton<IHostedService, BotStartupService>()
                .AddTransient<IReportTextRenderer, ReportTextRenderer>()
                .AddTransient<ICallbackValidator, CallbackValidator>()
                .AddTransient<IReportUpdater, ReportUpdater>()
                .AddTransient<IReportEntryCreator, ReportEntryCreator>()
                
                .AddTransient<IDbContextMigrator, DbContextMigrator<AnyMeterBotDataContext>>()
                
                .AddTransient<IHandler<Message>, ReportCommandHandler>()
                .AddTransient<IHandler<Message>, EmailSettingsCommandHandler>()
                .AddTransient<IHandler<Message>, SetValueHandler>()
                .AddTransient<IHandler<Message>, SetNameHandler>()
                .AddTransient<IHandler<ValidCallback>, AddEntry>()
                .AddTransient<IHandler<ValidCallback>, RemoveEntry>()
                .AddTransient<IHandler<ValidCallback>, Send>()
                .AddTransient<IHandler<ValidCallback>, EditValue>()
                .AddTransient<IHandler<ValidCallback>, EditName>()
                .AddTransient<IHandler<ValidCallback>, EditUnit>()
                
                .AddTransient<IMailSender, ElasticMailSender>()
                .AddTransient<IReportCreator, ReportCreator>()
                .AddTransient<IUserRepository, UserRepository>()
                .AddTransient<IMailMessageBuilder, MailMessageBuilder>()
                
                .AddTransient<IHandler<Update>, UpdateHandler>()
                .AddSingleton<ITelegramBotClient>(new TelegramBotClient(config[ConfigNames.TELEGRAM_BOT_TOKEN]));
        }

        [UsedImplicitly]
        public void Configure(IApplicationBuilder app, IApplicationLifetime lifetime)
        {
            app.UseMvc();

            using (var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                var migrationTasks = serviceScope.ServiceProvider
                    .GetServices<IDbContextMigrator>()
                    .Select(x => x.WaitForDbAndMigrate(lifetime.ApplicationStopping))
                    .ToArray();

                Task.WaitAll(migrationTasks);

            }
        }
    }
}