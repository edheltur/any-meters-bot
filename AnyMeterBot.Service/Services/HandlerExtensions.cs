﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AnyMeterBot.Service.Services
{
    public static class HandlerExtensions
    {
        public static async Task HandleAll<T>(this IEnumerable<IHandler<T>> handlers, T entity)
        {
            if (entity == null)
                return;

            foreach (var handler in handlers.Where(x => x.CanHandle(entity)))
                await handler.Handle(entity);
        }
    }
}