﻿using System;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Telegram.Bot.Types;

namespace AnyMeterBot.Service.Services
{
    public class CallbackValidator : ICallbackValidator
    {
        private readonly IAnyMeterDb db;

        public CallbackValidator(IAnyMeterDb db)
        {
            this.db = db;
        }

        [ItemCanBeNull]
        public Task<ValidCallback> Validate(CallbackQuery callbackQuery)
        {
            if (!Guid.TryParse(callbackQuery.Data, out var guid))
                return Task.FromResult<ValidCallback>(null);

            var reportCallback = db.ReportCallbacks.Find(guid);
            if (reportCallback == null)
                return Task.FromResult<ValidCallback>(null);
            
            return Task.FromResult(new ValidCallback(reportCallback, callbackQuery));
        }
    }
}