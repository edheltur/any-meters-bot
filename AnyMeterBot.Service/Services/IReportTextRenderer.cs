﻿using System;
using System.Collections.Generic;
using AnyMeterBot.Service.DataLayer;

namespace AnyMeterBot.Service.Services
{
    public interface IReportTextRenderer
    {
        string RenderAll(DateTime date, IList<ReportEntry> entries = null);
        
        
        string RenderBody(IList<ReportEntry> entries = null);
        string RenderTitle(DateTime date);
    }
}