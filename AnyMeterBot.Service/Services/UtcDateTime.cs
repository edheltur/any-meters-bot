﻿using System;

namespace AnyMeterBot.Service.Services
{
    public class UtcDateTime : IDateTime
    {
        public DateTime  Now => DateTime.UtcNow;
        public DateTime  UtcNow => DateTime.UtcNow;
    }
}