﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using AnyMeterBot.Service.DataLayer;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace AnyMeterBot.Service.Services
{
    public class EmailSettingsCommandHandler : IHandler<Message>
    {
        private readonly IAnyMeterDb db;
        private readonly ITelegramBotClient botClient;
        private readonly IUserRepository userRepository;


        public EmailSettingsCommandHandler(IAnyMeterDb db,
            ITelegramBotClient botClient,
            IUserRepository userRepository)
        {
            this.db = db;
            this.botClient = botClient;
            this.userRepository = userRepository;
        }

        private static readonly IReadOnlyDictionary<string, Action<EmailSettings, MailAddress>> SubCommands =
            new Dictionary<string, Action<EmailSettings, MailAddress>>
            {
                ["/email_to"] = (settings, value) => { settings.To = value; },
                ["/email_from"] = (settings, value) => { settings.From = value; },
            };

        private static readonly IReadOnlyList<Regex> SubCommandRegexps = SubCommands
            .Select(x => new Regex($"({x.Key})\\s(.+)", RegexOptions.Compiled))
            .ToArray();

        public bool CanHandle(Message message)
        {
            return message.EntityValues?.Any(x => SubCommands.Keys.Contains(x)) == true;
        }

        public async Task Handle(Message message, CancellationToken cancellationToken)
        {
            await botClient.SendChatActionAsync(message.Chat.Id, ChatAction.Typing, cancellationToken);
            var user = await userRepository.Get(message.From, cancellationToken);

            var settings = user.EmailSettings ?? new EmailSettings();


            foreach (var line in message.Text.SplitLines())
            foreach (var regex in SubCommandRegexps)
            {
                ProcessCommand(regex, line, settings);
            }

            var text = string.Join("\n",
                "Ваши email настройки:",
                $"To: {settings.To}",
                $"From: {settings.From}");
            await botClient.SendTextMessageAsync(message.Chat, text, cancellationToken: cancellationToken);

            user.EmailSettings = settings;

            await db.SaveChangesAsync(cancellationToken);
        }

        private static void ProcessCommand(Regex regex, string line, EmailSettings settings)
        {
            var match = regex.Match(line);
            if (match.Groups.Count < 2)
                return;

            var command = match.Groups[1].Value;
            var rawEmail = match.Groups[2].Value;


            if (string.IsNullOrEmpty(rawEmail))
                return;


            if (!MailAddressParser.TryParse(rawEmail, out var email))
                return;

            SubCommands[command](settings, email);
        }
    }
}