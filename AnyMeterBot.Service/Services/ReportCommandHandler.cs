﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;

namespace AnyMeterBot.Service.Services
{
    public class ReportCommandHandler : IHandler<Message>
    {
        private readonly IAnyMeterDb db;
        private readonly IReportCreator reportCreator;
        private readonly ITelegramBotClient botClient;
        private readonly IReportTextRenderer textRenderer;

        public ReportCommandHandler(IAnyMeterDb db, ITelegramBotClient botClient,
            IReportCreator reportCreator, IReportTextRenderer textRenderer)
        {
            this.db = db;
            this.botClient = botClient;
            this.reportCreator = reportCreator;
            this.textRenderer = textRenderer;
        }

        public bool CanHandle(Message message)
        {
            return message.EntityValues?.Any(x => x == "/report_new" || x == "/report_copy") == true;
        }

        
        public async Task Handle(Message message, CancellationToken cancellationToken)
        {
            await botClient.SendChatActionAsync(message.Chat.Id, ChatAction.Typing, cancellationToken);
            var createEmpty = message.EntityValues.Any(x => x == "/report_new");
            var report = await reportCreator.Create(message, createEmpty, cancellationToken);

            var reportMessage = await botClient.SendTextMessageAsync(
                message.Chat,
                textRenderer.RenderAll(report.CreationDate),
                replyMarkup: new InlineKeyboardMarkup(report.BuildButtons()),
                cancellationToken: cancellationToken);

            report.TelegramMessageId = reportMessage.MessageId;
            db.Reports.Add(report);
            await db.SaveChangesAsync(cancellationToken);
        }
    }
}