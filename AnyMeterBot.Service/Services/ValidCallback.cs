﻿using AnyMeterBot.Service.DataLayer;
using Telegram.Bot.Types;

namespace AnyMeterBot.Service.Services
{
    public class ValidCallback
    {
        public ValidCallback(ReportCallback reportCallback, CallbackQuery callbackQuery)
        {
            ReportCallback = reportCallback;
            CallbackQuery = callbackQuery;
        }

        public ReportCallback ReportCallback { get; }
        public CallbackQuery CallbackQuery { get; }
        
        
    }
}