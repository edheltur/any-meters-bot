﻿using System.Collections.Generic;
using System.Linq;
using AnyMeterBot.Service.DataLayer;
using Telegram.Bot.Types.ReplyMarkups;
using static AnyMeterBot.Service.DataLayer.ReportCallbackAction;

namespace AnyMeterBot.Service.Services
{
    public static class InlineKeyboardButtonExtensions
    {
        private static readonly IReadOnlyList<ReportCallbackAction> ReportRow =
            new[] {AddEntry, Send};

        private static readonly IReadOnlyList<ReportCallbackAction> EntryRow =
            new[] {EditName, EditValue, EditUnit, RemoveEntry};

        public static IEnumerable<IEnumerable<InlineKeyboardButton>> BuildButtons(this Report report)
        {
            yield return ReportRow.Select(x => BuildButton(x, report.Callbacks));
            foreach (var entry in report.Entries)
            {
                yield return EntryRow.Select(x => BuildButton(x, entry.Callbacks, entry));
            }
        }
        
        private static InlineKeyboardButton BuildButton(ReportCallbackAction templateItem, 
            IList<ReportCallback> callbacks, ReportEntry entry = null)
        {
            var callback = callbacks.FirstOrDefault(x => x.Action == templateItem);
            if (callback == null)
                return null;

            var title = callback.Action.GetTitle(entry);
            if (string.IsNullOrEmpty(title))
                return null;

            var button = InlineKeyboardButton.WithCallbackData(title, callback.ReportCallbackId.ToString());

            return button;
        }
    }
}