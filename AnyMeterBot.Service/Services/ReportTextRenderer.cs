﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using AnyMeterBot.Service.DataLayer;

namespace AnyMeterBot.Service.Services
{
    public class ReportTextRenderer : IReportTextRenderer
    {
        private const int LINE_ENDINGS_AFTER_BODY = 2;

        public string RenderAll(DateTime date, IList<ReportEntry> entries = null)
        {
            return $"{RenderTitle(date)}\n{RenderBody(entries)}";
        }

        public string RenderBody(IList<ReportEntry> entries = null)
        {
            entries = entries ?? new ReportEntry[0];
            var textEntries = entries
                .Select(ToTextLine)
                .Concat(Enumerable.Repeat(string.Empty, LINE_ENDINGS_AFTER_BODY));
            var joinedTextEntries = string.Join('\n', textEntries);
            return joinedTextEntries;
        }

        private string ToTextLine(ReportEntry x)
        {
            return $"{x.Name ?? "?"} {x.Value?.ToString() ?? "?"} {x.Unit.GetTitle()}";
        }

        public string RenderTitle(DateTime date)
        {
            var monthName = date.ToString("MMMM", CultureInfo.CreateSpecificCulture("ru"));
            return $"Показания счетчиков за {monthName} {date.Year}";
        }
    }
}