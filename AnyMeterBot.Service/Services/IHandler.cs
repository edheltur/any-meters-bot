﻿using System.Threading;
using System.Threading.Tasks;

namespace AnyMeterBot.Service.Services
{
    public interface IHandler<in T>
    {
        bool CanHandle(T entity);
        Task Handle(T entity, CancellationToken cancellationToken = default);
    }
}