﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types;

namespace AnyMeterBot.Service.Services
{
    public class BotStartupService : IHostedService
    {
        private readonly ITelegramBotClient botClient;
        private readonly IServiceScopeFactory serviceScopeFactory;
        private readonly ILogger<BotStartupService> logger;

        public BotStartupService(
            ITelegramBotClient botClient,
            IServiceScopeFactory serviceScopeFactory,
            ILogger<BotStartupService> logger)
        {
            this.botClient = botClient;
            this.serviceScopeFactory = serviceScopeFactory;
            this.logger = logger;
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            botClient.OnUpdate += OnBotClientOnOnUpdate;
            botClient.StartReceiving();
            await botClient.TestApiAsync(cancellationToken);
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            botClient.StopReceiving();
            botClient.OnUpdate -= OnBotClientOnOnUpdate;
            return Task.CompletedTask;
        }

        private async void OnBotClientOnOnUpdate(object sender, UpdateEventArgs args)
        {
            using (var scope = serviceScopeFactory.CreateScope())
            {
                try
                {
                    await scope.ServiceProvider
                        .GetServices<IHandler<Update>>()
                        .HandleAll(args.Update);

                }
                catch (AggregateException e)
                {
                    foreach (var exception in e.InnerExceptions)
                    {
                        LogUpdateError(args, exception);
                    }
                }
                catch (Exception exception)
                {
                    LogUpdateError(args, exception);
                }
            }
        }

        private void LogUpdateError(UpdateEventArgs args, Exception exception)
        {
            logger.LogError(args.Update.Id, exception, "Failed to process update!");
        }
    }
}