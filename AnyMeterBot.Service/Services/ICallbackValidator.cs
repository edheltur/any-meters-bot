﻿using System.Threading.Tasks;
using Telegram.Bot.Types;

namespace AnyMeterBot.Service.Services
{
    public interface ICallbackValidator
    {
        Task<ValidCallback> Validate(CallbackQuery callbackQuery);
    }
}