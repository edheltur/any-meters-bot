﻿using AnyMeterBot.Service.DataLayer;

namespace AnyMeterBot.Service.Services
{
    public interface IReportEntryCreator
    {
        ReportEntry Create();
    }
}