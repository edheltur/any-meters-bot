﻿using System;

namespace AnyMeterBot.Service.Services
{
    public interface IDateTime 
    {
        DateTime Now { get; }
        DateTime UtcNow { get; }
    }
}