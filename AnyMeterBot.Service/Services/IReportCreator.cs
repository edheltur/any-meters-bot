﻿using System.Threading;
using System.Threading.Tasks;
using AnyMeterBot.Service.DataLayer;
using Telegram.Bot.Types;

namespace AnyMeterBot.Service.Services
{
    public interface IReportCreator
    {
       Task<Report> Create(Message message, bool createEmpty, CancellationToken cancellationToken);
        
    }
}