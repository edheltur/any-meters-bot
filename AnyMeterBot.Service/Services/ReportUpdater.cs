﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AnyMeterBot.Service.DataLayer;
using Microsoft.EntityFrameworkCore;
using Telegram.Bot;
using Telegram.Bot.Exceptions;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;

namespace AnyMeterBot.Service.Services
{
    public class ReportUpdater : IReportUpdater
    {
        private readonly ITelegramBotClient botClient;
        private readonly IAnyMeterDb db;
        private readonly IReportTextRenderer textRenderer;

        public ReportUpdater(ITelegramBotClient botClient, IAnyMeterDb db, IReportTextRenderer textRenderer)
        {
            this.botClient = botClient;
            this.db = db;
            this.textRenderer = textRenderer;
        }

        public async Task Update(Message reportMessage, CancellationToken cancellationToken, Action<Report> update)
        {
            var report = await db.Reports
                .Include(x => x.Callbacks)
                .Include(x => x.Entries)
                    .ThenInclude(x=>x.Callbacks)
                .FirstAsync(x => x.TelegramMessageId == reportMessage.MessageId &&
                                 x.TelegramChatId == reportMessage.Chat.Id, cancellationToken);

            update(report);
            await SaveAndUpdateMessage(cancellationToken, report);
        }

        public async Task UpdateLast(CancellationToken cancellationToken, Action<Report> update)
        {
            var report = await db.Reports
                .Include(x => x.Callbacks)
                .Include(x => x.Entries)
                    .ThenInclude(x=>x.Callbacks)
                .OrderByDescending(x => x.CreationDate)
                .FirstOrDefaultAsync(cancellationToken);

            update(report);
            await SaveAndUpdateMessage(cancellationToken, report);
        }

        private async Task SaveAndUpdateMessage(CancellationToken cancellationToken, Report report)
        {
            await db.SaveChangesAsync(cancellationToken);

            try
            {
                await botClient.EditMessageTextAsync(
                    report.TelegramChatId,
                    report.TelegramMessageId,
                    text: textRenderer.RenderAll(report.CreationDate, report.Entries),
                    replyMarkup: new InlineKeyboardMarkup(report.BuildButtons()),
                    cancellationToken: cancellationToken);
            }
            catch (MessageIsNotModifiedException)
            {
               
            }
        }
    }
}