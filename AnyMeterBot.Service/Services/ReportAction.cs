﻿using AnyMeterBot.Service.DataLayer;
using Telegram.Bot.Types.ReplyMarkups;

namespace AnyMeterBot.Service.Services
{
    public class ReportAction
    {
        public readonly ReportCallback Callback;
        public readonly InlineKeyboardButton Button;

        public ReportAction(ReportCallback callback, InlineKeyboardButton button)
        {
            Callback = callback;
            Button = button;
        }
    }
}