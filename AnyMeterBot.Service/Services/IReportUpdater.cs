﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AnyMeterBot.Service.DataLayer;
using Telegram.Bot.Types;

namespace AnyMeterBot.Service.Services
{
    public interface IReportUpdater
    {
        Task Update(Message reportMessage, CancellationToken cancellationToken, Action<Report> update);
        Task UpdateLast(CancellationToken cancellationToken, Action<Report> update);
    }
}