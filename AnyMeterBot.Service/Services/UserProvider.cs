﻿using System.Threading;
using System.Threading.Tasks;
using AnyMeterBot.Service.DataLayer;
using Microsoft.EntityFrameworkCore;

namespace AnyMeterBot.Service.Services
{
    public class UserRepository : IUserRepository
    {
        private readonly IAnyMeterDb db;

        public UserRepository(IAnyMeterDb db)
        {
            this.db = db;
        }

        public async Task<User> Get(Telegram.Bot.Types.User telegramUser, CancellationToken cancellationToken)
        {
            var user = await db.Users
                .Include(x => x.EmailSettings)
                .FirstOrDefaultAsync(
                    x => x.TelegramUserId == telegramUser.Id,
                    cancellationToken);

            if (user != null)
                return user;

            user = new User {TelegramUserId = telegramUser.Id};
            db.Users.Add(user);
            
            return user;
        }
    }
}