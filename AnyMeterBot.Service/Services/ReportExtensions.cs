﻿using System;
using System.Collections.Generic;
using System.Linq;
using AnyMeterBot.Service.DataLayer;

namespace AnyMeterBot.Service.Services
{
    public static class ReportExtensions
    {
        public static IList<ReportEntry> CloneEntries(this Report report)
        {
            if (report == null)
                return new List<ReportEntry>();

            return report.Entries.Select(entry =>
                new ReportEntry
                {
                    Name = entry.Name,
                    Unit = entry.Unit,
                    Value = entry.Value,
                    Callbacks = entry.Callbacks
                        .Select(callaback => new ReportCallback
                        {
                            Action = callaback.Action,
                            ReportCallbackId = Guid.NewGuid()
                        }).ToArray()
                }).ToArray();
        }
    }
}