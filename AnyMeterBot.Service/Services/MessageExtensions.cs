﻿using Telegram.Bot.Types;

namespace AnyMeterBot.Service.Services
{
    public static class MessageExtensions
    {
        public static bool IsTextOnly(this Message message)
        {
            return message.Entities == null && !message.Text.Contains("\n");
        }
    }
}