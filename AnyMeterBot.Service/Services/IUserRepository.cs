﻿using System.Threading;
using System.Threading.Tasks;
using AnyMeterBot.Service.DataLayer;

namespace AnyMeterBot.Service.Services
{
    public interface IUserRepository
    {
        Task<User> Get(Telegram.Bot.Types.User telegramUser, CancellationToken cancellationToken = default);
    }
}