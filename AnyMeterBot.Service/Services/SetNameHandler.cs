﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AnyMeterBot.Service.Services.ReportActionHandlers;
using Telegram.Bot.Types;

namespace AnyMeterBot.Service.Services
{
    public class SetNameHandler : IHandler<Message>
    {
        private readonly IReportUpdater updater;

        public SetNameHandler(IReportUpdater updater)
        {
            this.updater = updater;
        }

        public bool CanHandle(Message message) => message.IsTextOnly();

        public Task Handle(Message message, CancellationToken cancellationToken) =>
            updater.UpdateLast(cancellationToken, report =>
            {
                foreach (var entry in report.Entries.Where(x=>x.Name == null))
                {
                    entry.Name = message.Text;
                }
            });
    }
}