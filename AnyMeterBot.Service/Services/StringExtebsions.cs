﻿using System;

namespace AnyMeterBot.Service.Services
{
    public static class StringExtebsions
    {
        public static string[] SplitLines(this string text)
        {
            return text.Split(new[] {"\r\n", "\r", "\n"}, StringSplitOptions.None);
        }
    }
}