﻿using System;
using System.Linq;
using AnyMeterBot.Service.DataLayer;
using static AnyMeterBot.Service.DataLayer.ReportCallbackAction;

namespace AnyMeterBot.Service.Services
{
    public class ReportEntryCreator : IReportEntryCreator
    {
        public ReportEntry Create()
        {
            var callbacks = new[] {EditName, EditValue, EditUnit, RemoveEntry}
                .Select(x => new ReportCallback {Action = x, ReportCallbackId = Guid.NewGuid()})
                .ToArray();
            
            return new ReportEntry {Name = "ХВС1", Value = 1337, Unit = Unit.kWh, Callbacks = callbacks};
        }
    }
}