﻿using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AnyMeterBot.Service.Services.ReportActionHandlers;
using Telegram.Bot.Types;

namespace AnyMeterBot.Service.Services
{
    public class SetValueHandler : IHandler<Message>
    {
        private readonly IReportUpdater updater;

        private const NumberStyles NUMBER_STYLES = NumberStyles.Float |
                                                   NumberStyles.AllowThousands |
                                                   NumberStyles.Integer;

        private static readonly NumberFormatInfo FormatInfo = NumberFormatInfo.InvariantInfo;

        public SetValueHandler(IReportUpdater updater)
        {
            this.updater = updater;
        }

        public bool CanHandle(Message message)
        {
            return message.IsTextOnly() && double.TryParse(message.Text, NUMBER_STYLES, FormatInfo, out _);
        }

        public Task Handle(Message message, CancellationToken cancellationToken) =>
            updater.UpdateLast(cancellationToken, report =>
            {
                foreach (var entry in report.Entries.Where(x => x.Value == null))
                {
                    entry.Value = double.Parse(message.Text, NUMBER_STYLES, FormatInfo);
                }
            });
    }
}