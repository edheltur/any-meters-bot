﻿using System.Net.Mail;
using AnyMeterBot.Service.DataLayer;

namespace AnyMeterBot.Service.Services
{
    public interface IMailMessageBuilder
    {
        MailMessage Build(EmailSettings settings, Report report);
    }
}