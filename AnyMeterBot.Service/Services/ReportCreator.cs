﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AnyMeterBot.Service.DataLayer;
using Microsoft.EntityFrameworkCore;
using Telegram.Bot.Types;
using static AnyMeterBot.Service.DataLayer.ReportCallbackAction;

namespace AnyMeterBot.Service.Services
{
    public class ReportCreator : IReportCreator
    {
        private readonly IDateTime dateTime;
        private readonly IUserRepository userRepository;
        private readonly IAnyMeterDb db;

        public ReportCreator(IDateTime dateTime, IUserRepository userRepository, IAnyMeterDb db)
        {
            this.dateTime = dateTime;
            this.userRepository = userRepository;
            this.db = db;
        }

        public async Task<Report> Create(Message message, bool createEmpty, CancellationToken cancellationToken)
        {
            var now = dateTime.UtcNow;
            var callbacks = new[] {AddEntry, Send}
                .Select(x => new ReportCallback {Action = x, ReportCallbackId = Guid.NewGuid()})
                .ToArray();

            var user = await userRepository.Get(message.From, cancellationToken);
            var report = new Report
            {
                CreationDate = now,
                Creator = user,
                TelegramChatId = message.Chat.Id,
                TelegramMessageId = message.MessageId,
                Callbacks = callbacks
            };

            if (!createEmpty)
            {
                var lastReport = await db.Reports
                    .Include(x => x.Entries)
                    .ThenInclude(x=>x.Callbacks)
                    .OrderByDescending(x => x.CreationDate)
                    .FirstOrDefaultAsync(x => x.CreatorId == user.UserId, cancellationToken);

                report.Entries = lastReport.CloneEntries();
            }
            return report;
        }
    }
}