﻿using System.Linq;
using System.Net.Mail;
using System.Threading;
using System.Threading.Tasks;
using ElasticEmailClient;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace AnyMeterBot.Service.Services.Mailing
{
    public class ElasticMailSender : IMailSender
    {
        public ElasticMailSender(IConfiguration config)
        {
            ElasticEmailClient.Api.ApiKey = config[ConfigNames.ELASTIC_EMAIL_API_KEY];
        }

        public async Task<MailSendResult> Send(MailMessage message, CancellationToken cancellationToken)
        {
            var sentMessage = await Api.Email.SendAsync(
                subject: message.Subject,
                from: message.From.ToString(),
                to: message.To.Select(x=>x.ToString()),
                replyTo: message.ReplyToList.Single().ToString(),
                bodyText: message.Body,
                isTransactional: true,
                trackClicks: false,
                trackOpens: true);

            var jsonData = JsonConvert.SerializeObject(sentMessage);
            return new MailSendResult(MailProvider.ElasicEmail, sentMessage.MessageID, jsonData);
        }
    }
}