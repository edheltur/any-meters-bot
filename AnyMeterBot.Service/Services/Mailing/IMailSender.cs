﻿using System.Net.Mail;
using System.Threading;
using System.Threading.Tasks;

namespace AnyMeterBot.Service.Services.Mailing
{
    public interface IMailSender
    {
        Task<MailSendResult> Send(MailMessage message, CancellationToken cancellationToken = default);
    }
}