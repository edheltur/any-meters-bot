﻿namespace AnyMeterBot.Service.Services.Mailing
{
    public class MailSendResult
    {
        public MailProvider Provider { get; }
        public string MessageId { get; }
        public string DataJson { get; }

        public MailSendResult(MailProvider provider, string messageId, string dataJson)
        {
            Provider = provider;
            MessageId = messageId;
            DataJson = dataJson;
        }
    }
}