﻿using System;
using System.Net.Mail;

namespace AnyMeterBot.Service.Services
{
    public static class MailAddressParser
    {
        public static bool TryParse(string value, out MailAddress email)
        {
            try
            {
                email = new MailAddress(value);
                return true;
            }
            catch (FormatException e)
            {
                email = null;
                return false;
            }
        }
    }
}