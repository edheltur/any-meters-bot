﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AnyMeterBot.Service.DataLayer;
using AnyMeterBot.Service.Services.Mailing;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.Configuration;
using Telegram.Bot;

namespace AnyMeterBot.Service.Services.ReportActionHandlers
{
    public class Send : IHandler<ValidCallback>
    {
        private readonly ITelegramBotClient botClient;
        private readonly IMailSender mailSender;
        private readonly IAnyMeterDb db;
        private readonly IMailMessageBuilder mailMessageBuilder;
        private readonly IConfiguration config;


        public Send(ITelegramBotClient botClient, IMailSender mailSender, IAnyMeterDb db,
            IMailMessageBuilder mailMessageBuilder, IConfiguration config)
        {
            this.botClient = botClient;
            this.mailSender = mailSender;
            this.db = db;
            this.mailMessageBuilder = mailMessageBuilder;
            this.config = config;
        }

        public bool CanHandle(ValidCallback callback) =>
            callback.ReportCallback.Action == ReportCallbackAction.Send;

        public async Task Handle(ValidCallback callaback, CancellationToken cancellationToken)
        {
            var message = callaback.CallbackQuery.Message;
            var report = await db.Reports
                .Include(x => x.Entries)
                .Include(x => x.Creator)
                .ThenInclude(x => x.EmailSettings)
                .FirstAsync(x => x.TelegramMessageId == message.MessageId &&
                                 x.TelegramChatId == message.Chat.Id,
                    cancellationToken);


            var settings = report.Creator.EmailSettings;

            var isEmailingAllowed = config.GetSection(ConfigNames.USERS_ALLOWED_TO_SEND_EMAIL)
                .Get<List<string>>()
                .Contains(callaback.CallbackQuery.From.Username);
            if (!isEmailingAllowed)
            {
                await Reply("Отправка доступна только проверенным пользователям!");
                return;
            }
            
            if (settings?.To == null || settings.From == null)
            {
                await Reply("У вас не настроены email для отправки!");
                return;
            }

            var mailMessage = mailMessageBuilder.Build(settings, report);

            await mailSender.Send(mailMessage, cancellationToken);


            await Reply("Готово!");


            async Task Reply(string text)
            {
                await botClient.AnswerCallbackQueryAsync(callaback.CallbackQuery.Id,
                    text, cancellationToken: cancellationToken);
            }
        }
    }
}