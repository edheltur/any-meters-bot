﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AnyMeterBot.Service.DataLayer;
using Telegram.Bot;

namespace AnyMeterBot.Service.Services.ReportActionHandlers
{
    public class EditValue : IHandler<ValidCallback>
    {
        private readonly IReportUpdater updater;
        private readonly ITelegramBotClient botClient;

        public EditValue(IReportUpdater updater, ITelegramBotClient botClient)
        {
            this.updater = updater;
            this.botClient = botClient;
        }

        public bool CanHandle(ValidCallback callback) =>
            callback.ReportCallback.Action == ReportCallbackAction.EditValue;

        public Task Handle(ValidCallback callaback, CancellationToken cancellationToken)
        {
            botClient.AnswerCallbackQueryAsync(
                callaback.CallbackQuery.Id,
                "Введите значение",
                cancellationToken: cancellationToken);

            return updater.Update(callaback.CallbackQuery.Message, cancellationToken,
                report =>
                {
                    var entry = report.Entries
                        .First(x => x.Callbacks.Contains(callaback.ReportCallback));
                    entry.Value = null;
                });
        }
    }
}