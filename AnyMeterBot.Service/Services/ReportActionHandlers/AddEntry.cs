﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AnyMeterBot.Service.DataLayer;

namespace AnyMeterBot.Service.Services.ReportActionHandlers
{
    public class AddEntry : IHandler<ValidCallback>
    {
        private readonly IReportUpdater updater;
        private readonly IReportEntryCreator reportEntryCreator;

        public AddEntry(IReportUpdater updater, IReportEntryCreator reportEntryCreator)
        {
            this.updater = updater;
            this.reportEntryCreator = reportEntryCreator;
        }

        public bool CanHandle(ValidCallback callback) =>
            callback.ReportCallback.Action == ReportCallbackAction.AddEntry;

        public Task Handle(ValidCallback callaback, CancellationToken cancellationToken) =>
            updater.Update(callaback.CallbackQuery.Message, cancellationToken,
                report => { report.Entries.Add(reportEntryCreator.Create()); });
    }
}