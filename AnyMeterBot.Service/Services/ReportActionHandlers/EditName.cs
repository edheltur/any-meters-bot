﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AnyMeterBot.Service.DataLayer;
using Telegram.Bot;

namespace AnyMeterBot.Service.Services.ReportActionHandlers
{
    public class EditName : IHandler<ValidCallback>
    {
        private readonly IReportUpdater updater;
        private readonly ITelegramBotClient botClient;

        public EditName(IReportUpdater updater, ITelegramBotClient botClient)
        {
            this.updater = updater;
            this.botClient = botClient;
        }

        public bool CanHandle(ValidCallback callback) =>
            callback.ReportCallback.Action == ReportCallbackAction.EditName;

        public Task Handle(ValidCallback callaback, CancellationToken cancellationToken)
        {
            botClient.AnswerCallbackQueryAsync(
                callaback.CallbackQuery.Id,
                "Введите имя",
                cancellationToken: cancellationToken);

            return updater.Update(callaback.CallbackQuery.Message, cancellationToken,
                report =>
                {
                    var entry = report.Entries
                        .First(x => x.Callbacks.Contains(callaback.ReportCallback));
                    entry.Name = null;
                });
        }
    }
}