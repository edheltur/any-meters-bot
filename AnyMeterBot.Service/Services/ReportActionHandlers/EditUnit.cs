﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AnyMeterBot.Service.DataLayer;
using Telegram.Bot;

namespace AnyMeterBot.Service.Services.ReportActionHandlers
{
    public class EditUnit : IHandler<ValidCallback>
    {
        private readonly IReportUpdater updater;
  

        public bool CanHandle(ValidCallback callback) =>
            callback.ReportCallback.Action == ReportCallbackAction.EditUnit;


        public EditUnit(IReportUpdater updater)
        {
            this.updater = updater;
        }


        public Task Handle(ValidCallback callaback, CancellationToken cancellationToken) =>
            updater.Update(callaback.CallbackQuery.Message, cancellationToken,
                report =>
                {
                    var entry = report.Entries
                        .First(x => x.Callbacks.Contains(callaback.ReportCallback));
                    entry.Unit = entry.Unit.Next();
                });
    }
}