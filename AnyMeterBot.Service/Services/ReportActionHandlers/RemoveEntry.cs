﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AnyMeterBot.Service.DataLayer;

namespace AnyMeterBot.Service.Services.ReportActionHandlers
{
    public class RemoveEntry : IHandler<ValidCallback>
    {
        private readonly IReportUpdater updater;

        public RemoveEntry(IReportUpdater updater)
        {
            this.updater = updater;
        }

        public bool CanHandle(ValidCallback callback) =>
            callback.ReportCallback.Action == ReportCallbackAction.RemoveEntry;

        public Task Handle(ValidCallback callaback, CancellationToken cancellationToken) =>
            updater.Update(callaback.CallbackQuery.Message, cancellationToken,
                report =>
                {
                    var entry = report.Entries.First(x => x.Callbacks.Contains(callaback.ReportCallback));
                    report.Entries.Remove(entry);
                });
    }
}