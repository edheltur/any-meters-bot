﻿using System.Net.Mail;
using AnyMeterBot.Service.DataLayer;
using AnyMeterBot.Service.Services.ReportActionHandlers;
using Microsoft.Extensions.Configuration;

namespace AnyMeterBot.Service.Services
{
    public class MailMessageBuilder : IMailMessageBuilder
    {
        private readonly IReportTextRenderer renderer;
        private readonly IConfiguration config;

        public MailMessageBuilder(IReportTextRenderer renderer, IConfiguration config)
        {
            this.renderer = renderer;
            this.config = config;
        }

        public MailMessage Build(EmailSettings settings, Report report)
        {
            var mailMessage = new MailMessage(settings.From, settings.To)
            {
                Subject = renderer.RenderTitle(report.CreationDate),
                Body = renderer.RenderBody(report.Entries),
                ReplyToList = { settings.From }
            };

            return mailMessage;
        }
    }
}