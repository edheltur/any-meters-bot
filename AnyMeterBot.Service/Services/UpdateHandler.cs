﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace AnyMeterBot.Service.Services
{
    public class UpdateHandler : IHandler<Update>
    {
        private readonly ILogger<UpdateHandler> logger;
        private readonly ITelegramBotClient botClient;
        private readonly IEnumerable<IHandler<Message>> messageHandlers;
        private readonly ICallbackValidator callbackValidator;
        private readonly IEnumerable<IHandler<ValidCallback>> callbackQueryHandlers;

        public UpdateHandler(ILogger<UpdateHandler> logger, ITelegramBotClient botClient,
            IEnumerable<IHandler<Message>> messageHandlers,
            IEnumerable<IHandler<ValidCallback>> callbackQueryHandlers,
            ICallbackValidator callbackValidator)
        {
            this.logger = logger;
            this.botClient = botClient;
            this.messageHandlers = messageHandlers;
            this.callbackQueryHandlers = callbackQueryHandlers;
            this.callbackValidator = callbackValidator;
        }


        public Task Handle(Update update, CancellationToken cancellationToken = default)
        {
            switch (update.Type)
            {
                case UpdateType.Message:
                    return messageHandlers.HandleAll(update.Message);
                case UpdateType.CallbackQuery:
                    return HandleCallbackQuery(update.CallbackQuery);
                default:
                    throw new Exception("Usupported update type!");
            }
        }

        private async Task HandleCallbackQuery(CallbackQuery callbackQuery)
        {
            var validCallback = await callbackValidator.Validate(callbackQuery);
            await callbackQueryHandlers.HandleAll(validCallback);
        }

        public bool CanHandle(Update update)
        {
            return SupportedUpdateTypes.Contains(update.Type);
        }

        private static readonly IReadOnlyList<UpdateType> SupportedUpdateTypes =
            new[] {UpdateType.Message, UpdateType.CallbackQuery};
    }
}