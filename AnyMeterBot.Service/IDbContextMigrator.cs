﻿using System.Threading;
using System.Threading.Tasks;

namespace AnyMeterBot.Service
{
    public interface IDbContextMigrator
    {
        Task WaitForDbAndMigrate(CancellationToken cancellationToken=default);
    }
}