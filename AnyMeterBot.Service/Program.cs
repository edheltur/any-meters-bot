﻿using System.IO;
using AnyMeterBot.Service.Logs;
using JetBrains.Annotations;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace AnyMeterBot.Service
{
  public static class Program
    {
        public static void Main(string[] args)
        {
            BuildWebHost(args).Run();
        }

        [UsedImplicitly]
        public static IWebHost BuildWebHost(string[] args) =>
            new WebHostBuilder()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .ConfigureAppConfiguration((context, config) => config
                    .AddJsonFile("appSettings.json")
                    .AddJsonFile($"appSettings.{context.HostingEnvironment.EnvironmentName}.json")
                    .AddEnvironmentVariables()
                    .AddCommandLine(args))
                
                .ConfigureLogging((context, logging) => logging
                    .AddConfiguration(context.Configuration.GetSection("Logging"))
                    .AddJsonConsole()
                    .AddConsole())
                
                .UseDefaultServiceProvider((context, options) =>
                    options.ValidateScopes = context.HostingEnvironment.IsDevelopment())
                
                .UseKestrel()
                .UseStartup<Startup>()
                .Build();
    }
}