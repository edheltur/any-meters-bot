﻿using Microsoft.Extensions.Logging.Configuration;
using Microsoft.Extensions.Logging.Console;
using Microsoft.Extensions.Options;

namespace AnyMeterBot.Service.Logs
{
    internal class JsonConsoleLoggerOptionsSetup : ConfigureFromConfigurationOptions<JsonConsoleLoggerOptions>
    {
        public JsonConsoleLoggerOptionsSetup(ILoggerProviderConfiguration<JsonConsoleLoggerProvider> providerConfiguration)
            : base(providerConfiguration.Configuration)
        {
            ;
        }
    }
}