﻿using System;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Configuration;
using Microsoft.Extensions.Logging.Console;
using Microsoft.Extensions.Options;
using static Microsoft.Extensions.DependencyInjection.ServiceDescriptor;

namespace AnyMeterBot.Service.Logs
{
    public static class JsonConsoleLoggerExtensions
    {
        public static ILoggingBuilder AddJsonConsole(
            this ILoggingBuilder builder,
            Action<JsonConsoleLoggerOptions> configure = null)
        {
            builder.AddConfiguration();
            if (configure != null)
            {
                builder.Services.Configure(configure);
            }

            builder.Services.TryAddEnumerable(Singleton<ILoggerProvider, JsonConsoleLoggerProvider>());
            builder.Services.TryAddEnumerable(
                Singleton<IConfigureOptions<JsonConsoleLoggerOptions>, JsonConsoleLoggerOptionsSetup>());
           
            return builder;
        }
    }
}