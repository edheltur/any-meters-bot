﻿using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace AnyMeterBot.Service.Logs
{
    public class JsonConsoleLoggerOptions
    {
        public Formatting Formatting { get; set; } = Formatting.None;
    }
}