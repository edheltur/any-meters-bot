﻿using System;
using System.Collections;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace AnyMeterBot.Service.Logs
{
    public struct JsonLogMessage
    {
        public JsonLogMessage(LogLevel level, string message, object messageData, EventId eventId, Exception exception)
        {
            Level = level;
            Message = message;
            MessageData = messageData;
            EventId = eventId;
            Exception = exception;

        }

        [JsonProperty("level")]
        public LogLevel Level { get; }
        
        [JsonProperty("message")]
        public string Message { get; }
        
        [JsonProperty("messageData")]
        public object MessageData { get; }
        
        [JsonProperty("exception")]
        public Exception Exception { get; }
        
        [JsonProperty("eventId")]
        public EventId EventId { get; }
        
    }
}