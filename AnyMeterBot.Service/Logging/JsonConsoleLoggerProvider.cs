﻿using System.Collections.Concurrent;
using AnyMeterBot.Service.Logging;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace AnyMeterBot.Service.Logs
{
    [ProviderAlias("JsonConsole")]
    public class JsonConsoleLoggerProvider : ILoggerProvider
    {

        private readonly IOptionsMonitor<JsonConsoleLoggerOptions> optionsMonitor;

        private readonly ConcurrentDictionary<string, JsonConsoleLogger> loggers;
            

        public JsonConsoleLoggerProvider(IOptionsMonitor<JsonConsoleLoggerOptions> optionsMonitor)
        {
            this.optionsMonitor = optionsMonitor;
            this.loggers = new ConcurrentDictionary<string, JsonConsoleLogger>();
        }


        public ILogger CreateLogger(string categoryName)
        {
            return loggers.GetOrAdd(categoryName, name => new JsonConsoleLogger(name, optionsMonitor.CurrentValue));
        }

        public void Dispose()
        {
            loggers.Clear();
        }
    }
}