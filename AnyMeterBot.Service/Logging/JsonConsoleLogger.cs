﻿using System;
using System.Collections.Generic;
using System.Globalization;
using AnyMeterBot.Service.Logs;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace AnyMeterBot.Service.Logging
{
    public class JsonConsoleLogger : ILogger
    {
        private readonly string name;
        private readonly JsonConsoleLoggerOptions config;
        private readonly JsonSerializer jsonSerializer;

        public JsonConsoleLogger(string name, JsonConsoleLoggerOptions config)
        {
            this.name = name;
            this.config = config;
            this.jsonSerializer = new JsonSerializer
            {
                Culture = CultureInfo.InvariantCulture,
                Formatting = config.Formatting,
                NullValueHandling = NullValueHandling.Ignore
            };
        }

        public IDisposable BeginScope<TState>(TState state)
        {
            return null;
        }

        public bool IsEnabled(LogLevel logLevel)
        {
            return logLevel != LogLevel.None;
        }

        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception,
            Func<TState, Exception, string> formatter)
        {
            if (!IsEnabled(logLevel))
                return;

            var message = formatter(state, exception);
            var messageData = RecognizeMessageData(state);
            var logMessage = new JsonLogMessage(logLevel, message, messageData, eventId, exception);
            jsonSerializer.Serialize(Console.Out, logMessage);
            Console.Out.WriteLine();
            Console.Out.Flush();
        }

        private static object RecognizeMessageData<TState>(TState state)
        {
            try
            {
                if (state is IEnumerable<KeyValuePair<string, object>> keyValueCollection)
                {
                    return new Dictionary<string, object>(keyValueCollection);
                }
            }
            catch (Exception)
            {
                return state;
            }

            return state;
        }
    }
}