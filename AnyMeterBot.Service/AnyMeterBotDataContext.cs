﻿using System;
using System.Net.Mail;
using AnyMeterBot.Service.DataLayer;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace AnyMeterBot.Service
{
    public class AnyMeterBotDataContext : DbContext, IAnyMeterDb
    {

        public AnyMeterBotDataContext(DbContextOptions<AnyMeterBotDataContext> options)
            : base(options)
        {
        }

        public DbSet<Report> Reports { get; set; }
        public DbSet<ReportCallback> ReportCallbacks { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
              
            var converter = new ValueConverter<MailAddress, string>(
                v => v.ToString(),
                v => new MailAddress(v));

            modelBuilder
                .Entity<EmailSettings>()
                .Property(e => e.From)
                .HasConversion(converter);
            
            modelBuilder
                .Entity<EmailSettings>()
                .Property(e => e.To)
                .HasConversion(converter);

        }
    }
}